<?php $title_page = "Inscription";
include './header.php'; ?>
<!-- <?php include '../include/config.inc.php'; ?>
<?php include '../include/form.php'; ?> -->

<section class="sectionInscription--header">
	<h2 class="sectionInscription--title">Inscription</h2>
</section>

<section class="sectionForm">
	<div class="sectionForm__civilite">
		<label for="civilite" class="sectionForm__label">Civilité</label>
		<select id="civilité" class="sectionForm__input sectionForm__input--medium">
			<option value="madame">Madame</option>
			<option value="monsieur">Monsieur</option>
			<option value="autre">Autre</option>
		</select>
	</div>
	<div class="sectionForm__container">
		<form method="post" action="inscription2.php">
		<div class="sectionForm__container-name">
			<div class="sectionForm__container-name-1">
				<label for="nom" class="sectionForm__label">Nom</label>
				<input id="nom" type="text" name="nom" value="<?php echo $nom; ?>">
			</div>
			<div class="sectionForm__container-name-1">
				<label for="prenom" class="sectionForm__label">Prénom</label>
				<input id="prenom" type="text" name="prenom" value="<?php echo $prenom; ?>">
			</div>
		</div>
		<div class="sectionForm__container-date">
				<label>Date de Naissance</label>
				<input type="date" name="date" id="date">
		</div>

		<div class="sectionForm__container-tel">
			<label>Téléphone</label>
			<input type="text" name="tel" value="<?php echo $tel; ?>">
		</div>

		<div class="sectionForm__container-ville">
			<div class="sectionForm__container-ville-1">
				<label class="sectionForm__label">Ville</label>
				<input id="ville" type="text" name="ville" value="<?php echo $ville; ?>">
			</div>
			<div class="sectionForm__container-ville-1">
				<label for="prenom" class="sectionForm__label">Pays</label>
				<input id="pays" type="text" name="pays" value="<?php echo $pays; ?>">
			</div>
		</div>
		<div class="sectionForm__container-mail">
			<label>Email</label>
			<input type="email" name="mail" value="<?php echo $mail; ?>">
		</div>
		<div class="sectionForm__container-password">
			<label>Mot de passe</label>
			<input type="password" name="password_1" value="">
		</div>
		<div class="sectionForm__container-password2">
			<label>Confirmer mot de passe</label>
			<input type="password" name="password_2">
		</div>
		<div class="sectionErrors">
			<?php echo show_error(); ?>
		</div>
	</div>
</section>
<div class="sectionButton">
	<button type="submit" class="sectionButton_item" name="btn"><a href="./inscription2.php">Suivant</a></button>
</div>
</form>
<script type="text/javascript" src="../scripts/index.js"></script>
</body>

</html>
