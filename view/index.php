<?php $title_page = 'Acad\'EEMI'; ?>
<?php include './header.php'; ?>
<div class="sectionInscription">
	<h2 class="sectionInscription__title">Acad'eemi</h2>
	<a href="./inscription.php"><button class="sectionInscription__button">Je m'inscris !</button></a>
</div>
<div class="sectionConcept">
	<img src="../assets/images/palais-img.jpg" class="sectionConcept__img" alt="Une photo du palais Brongniart">
	<div class="sectionConcept__texte">
		<h4 class="sectionConcept__title">Notre Concept</h4>
		<p class="sectionConcept__paragraphe">Nous proposons une plateforme de service de formation en ligne, basé sur les cours de l'EEMI. Sur cette plateforme, des cours d'E-business, de Web Developpement ainsi que d'Interactive Design te seront proposés !</p>
	</div>
</div>
<div class="sectionConcept">
	<img src="../assets/images/eemi-img.jpg" class="sectionConcept__img" alt="Une photo de nos locaux">
	<div class="sectionConcept__texte">
		<h4 class="sectionConcept__title">Notre école</h4>
		<p class="sectionConcept__paragraphe">Free, Meetic et Vente-privee.com. Les fondateurs de ces trois entreprises, leaders du Net en Europe, ont décidé de créer ensemble l’EEMI pour répondre à leurs besoins propres ainsi qu’à ceux d’une profession porteuse d’avenir et en constante (r)évolution.</p>
		<p>En savoir plus</p>
	</div>
</div>
<script type="text/javascript" src="../scripts/index.js"></script>
</body>
</html>
