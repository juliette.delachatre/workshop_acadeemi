<?php include './header.php'; ?>
<section class="sectionCours">
	<h2 class="sectionCours__title">Les cours</h2>
</section>
<section class="sectionCategorie">
<div class="barreXp"><div>
    <div class="barreXpOrange">
</div>
</section>
<section class="sectionDev">
    
    <div>
        <h2 class= "tittleCours1">WEB DEVELOPPEMENT</h2>
    </div>
    <div class= "divCategorie1">
        <h3 class="divCategorieTexte">Introduction à l'HTML/CSS</h3>
        <div><p id="clickB-1" class ="texteCarte hidden">tamere<p></div>
        <img class="fleche_menu" id="clickB-2" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        <div class="clickCategorie" onclick="showTexte()">
            </div>
    </div>
    <div class= "divCategorie2">
        <h3 class="divCategorieTexte">Le langage PHP</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
    <div class= "divCategorie3">
        <h3 class="divCategorieTexte">Les bases du JavaScript</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
</section>
<section>
    <div class= "tittleCours2">
        <h2>INTERACTIVE DESIGN</h2>
    </div>
    <div class= "divCategorie1">
        <h3 class="divCategorieTexte">Les bonnes pratiques</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
    <div class= "divCategorie2">
        <h3 class="divCategorieTexte">Les logiciels</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
    <div class= "divCategorie3">
        <h3 class="divCategorieTexte">Les annexes</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
</section>
<section>
    <div class= "tittleCours3">
        <h2>E-BUSINESS</h2>
    </div>
    <div class= "divCategorie1">
        <h3 class="divCategorieTexte">Les résaux sociaux</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
    <div class= "divCategorie2">
        <h3 class="divCategorieTexte">Analyse d’un marché</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
        </div>
    </div>
    <div class= "divCategorie3">
        <h3 class="divCategorieTexte">La segmentation et le persona</h3>
        <img  class="fleche_menu" src="../assets/images/fleche_bas.png" alt="Fleche vers le bas">
</div>
    </div>
</section>
<script type="text/javascript" src="../scripts/index.js"></script>
</body>
</html>
