<?php 
$host = 'localhost';
$user = 'root';
$password = '';
$bdd = 'acadeemi';

$lien = mysqli_connect($host, $user, $password, $bdd);
mysqli_set_charset($lien, 'utf8');
$categorie_req = 'SELECT categorie_nom FROM categorie WHERE id_categorie=' . $categorie;
$article_req = 'SELECT * FROM article WHERE id_article=' . $article;
$section_req = 'SELECT section_contenu FROM section WHERE id_section=' . $section;
$query_cat = mysqli_query($lien, $categorie_req);
$query_article = mysqli_query($lien, $article_req);
$query_section = mysqli_query($lien, $section_req);
$result_cat = mysqli_fetch_assoc($query_cat);
$result_article = mysqli_fetch_assoc($query_article);
$result_section = mysqli_fetch_assoc($query_section);
$title_page = $result_cat['categorie_nom'] . ' - ' . $result_article['article_titre'];
include './header.php';
?>

<section class="sectionArticle">
	<a class="sectionArticle__back" href="./cours.php"><- Retour</a>
	<h2 class="sectionArticle__cat"><?php echo $result_cat['categorie_nom'] ;?></h2>
	<h4 class="sectionArticle__nomArt"><?php echo $result_article['article_titre'] ;?></h4>

<?php echo $result_section['section_contenu']; ?>
<script type="text/javascript" src="../scripts/index.js"></script>