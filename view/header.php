<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="../styles/style.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<title><?php echo $title_page; ?></title>
</head>
<body>
	<div id="hidden" class="sectionMenu">
		<div>
			<p onclick="menu('hide')" class="sectionMenu__back"><- Retour</p>
			<div id="menu" class="sectionMenu__container">
				<p onclick="connect()" class="sectionMenu__link">Se connecter</p>
				<a class="sectionMenu__link" href="./inscription.php">S'inscrire</a>
				<a class="sectionMenu__link" href="./index.php">Acceuil</a>
				<a class="sectionMenu__link" href="./cours.php">Les cours</a>
			</div>
			<div id="connect" class="sectionMenu__connect">
				<p class="sectionMenu__title">Se connecter</p>
				<input class="sectionMenu__input" type="text" name="email" placeholder="E-mail">
				<input class="sectionMenu__input" type="text" name="email" placeholder="Mot de passe">
				<button class="sectionMenu__button">Connexion</button>
				<div class="sectionMenu__google--container">
					<img  class="sectionMenu__google--logo" src="../assets/images/logo-google.png" alt="Logo de Google">
					<p class="sectionMenu__google--texte">Avec Google</p>
				</div>
			</div>
		</div>
	</div>
	<header class="sectionHeader">
		<img class="sectionHeader__img" src="../assets/images/logo-acadeemi.png" alt="Une photo de notre logo - ACAD'EEMI">
		<div onclick="menu('show')" class="sectionHeader__menu--container">
			<div class="sectionHeader__menu--item"></div>
			<div class="sectionHeader__menu--item"></div>
			<div class="sectionHeader__menu--item"></div>
		</div>
	</header>